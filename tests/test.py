import os, sys
sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/../')

import unittest
import vanityonionr

import mnemonic
m = mnemonic.Mnemonic("english")
wordlist = m.wordlist

class TestBasic(unittest.TestCase):

    def test_basic(self):
        pair = vanityonionr.find_multiprocess("onion")
        b = m.to_mnemonic(pair[0])
        print(b)
        self.assertIn("onion", b)

unittest.main()
from setuptools import setup, find_packages

setup(name='vanityonionr',
      version='0.0.0',
      description='Generate vanity keys for Onionr',
      author='Kevin Froman',
      author_email='beardog@mailbox.org',
      url='https://github.com/beardog108/vanity-onionr/',
      packages=['vanityonionr'],
      install_requires=["mnemonic>=0.18", "PyNaCl>=1.3.0"],
      classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
      ],
     )
